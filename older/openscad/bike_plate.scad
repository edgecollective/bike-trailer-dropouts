// CSG.scad - Basic example of CSG usage


width = 100;
height = 60;
mount_x = 30;
mount_y = 15;
mount_width=7;
mount_height=12;
dropout_width=10;
dropout_height=10;
rounded_radius=5;

projection() difference() {
   
    //translate([5,5,0])
    minkowski() {
        cube([width,height,1],center=true);
        cylinder(r=rounded_radius,h=1.4,center=true);
    }
    
    
    //ruler
    translate([mount_x+7,0,0])
    cube([mount_width,13,30],center=true);
    
    // upper right
    union() {
        translate([mount_x,mount_y,0])
        cube([mount_width,mount_height-mount_width,30],center=true);
    
        translate([mount_x,mount_y+mount_height-2*mount_width,0])
        cylinder(h=10,r=mount_width/2,center=true,$fn=100);
        
        translate([mount_x,mount_y-mount_height+2*mount_width,0])
        cylinder(h=10,r=mount_width/2,center=true,$fn=100);
    }

    // lower right
     union() {
        translate([mount_x,-mount_y,0])
        cube([mount_width,mount_height-mount_width,30],center=true);
    
        translate([mount_x,-mount_y+mount_height-2*mount_width,0])
        cylinder(h=10,r=mount_width/2,center=true,$fn=100);
        
        translate([mount_x,-mount_y-mount_height+2*mount_width,0])
        cylinder(h=10,r=mount_width/2,center=true,$fn=100);
    }
    
    // upper left
      union() {
        translate([-mount_x,mount_y,0])
        cube([mount_width,mount_height-mount_width,30],center=true);
    
        translate([-mount_x,mount_y+mount_height-2*mount_width,0])
        cylinder(h=10,r=mount_width/2,center=true,$fn=100);
        
        translate([-mount_x,mount_y-mount_height+2*mount_width,0])
        cylinder(h=10,r=mount_width/2,center=true,$fn=100);
    }
    
    // lower left
          union() {
        translate([-mount_x,-mount_y,0])
        cube([mount_width,mount_height-mount_width,30],center=true);
    
        translate([-mount_x,-mount_y+mount_height-2*mount_width,0])
        cylinder(h=10,r=mount_width/2,center=true,$fn=100);
        
        translate([-mount_x,-mount_y-mount_height+2*mount_width,0])
        cylinder(h=10,r=mount_width/2,center=true,$fn=100);
    }
    
    
    // dropout
    
    //neck
    union() {
    translate([0,-height/2+dropout_height/2-rounded_radius,0])
    //rotate([0,0,45])
    cube([dropout_width,dropout_height,30],center=true);
    
    //head
    translate([0,-height/2+dropout_height-rounded_radius,0])
    cylinder(h=10,r=dropout_width/2,center=true,$fn=100); 
        
        
    }
    
    }
    
    

echo(version=version());
// Written by Marius Kintel <marius@kintel.net>
//
// To the extent possible under law, the author(s) have dedicated all
// copyright and related and neighboring rights to this software to the
// public domain worldwide. This software is distributed without any
// warranty.
//
// You should have received a copy of the CC0 Public Domain
// Dedication along with this software.
// If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
